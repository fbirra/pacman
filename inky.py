import monster
from spritesheet import SpriteSheet

class Inky(monster.Monster):
    sprite_sheet = SpriteSheet("images/spritesheet.png")
    inky_animations = monster.init_monster_animations(sprite_sheet, 2)

    def __init__(self, x, y):
        super().__init__(x,y,self.inky_animations, monster.MONSTER_UP)
