import pygame
import constants
from actor import Actor

class AnimatedActor(Actor):
    def __init__(self, x, y, animations, initial_state):
        super().__init__(x,y,animations[initial_state][0])
        #
        self.animations = animations
        self.state = None
        self.is_animated = False
        self.start_animation(initial_state)
        self.frames_counter = 0
        self.rect = pygame.Rect(x+4,y+4,8,8)

    def start_animation(self, state):
        # if we are already in the right state, exit
        if(self.state == state):
            return
        self.state = state
        self.img_index = 0
        self.is_animated = True
        self.update_image()

    def stop_animation(self):
        self.is_animated = False

    def update_image(self):
        self.image = self.animations[self.state][self.img_index]

    def is_animation_finished(self):
        return self.img_index == len(self.animations[self.state]) - 1

    def advance_frame(self):
        self.img_index = (self.img_index + 1) % len(self.animations[self.state])
        self.update_image()

    def set_obstacles(self, obstacles):
        self.obstacles = obstacles

    def move_up(self, curr_state):
        self.wants_to_go_up = curr_state

    def move_down(self, curr_state):
        self.wants_to_go_down = curr_state

    def move_left(self, curr_state):
        self.wants_to_go_left = curr_state

    def move_right(self, curr_state):
        self.wants_to_go_right = curr_state

    def will_collide(self, dx, dy):
        self.rect.x += dx
        self.rect.y += dy
        hit_list = pygame.sprite.spritecollide(self, self.obstacles, False)
        self.rect.x -= dx
        self.rect.y -= dy
        return len(hit_list) > 0

    def can_go_up(self):
        return not self.will_collide(0,-1)

    def can_go_down(self):
        return not self.will_collide(0,1)

    def can_go_left(self):
        return not self.will_collide(-1,0)

    def can_go_right(self):
        return not self.will_collide(1,0)

    def update(self):
        if not self.is_animated:
            return
        self.frames_counter+=1
        if(self.frames_counter==constants.FRAMES_PER_SPRITE):
            self.frames_counter = 0
            self.advance_frame()

    def draw(self, screen):
        screen.blit(self.image, (self.rect.x-3,self.rect.y-3))
