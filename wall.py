from actor import Actor

class Wall(Actor):
    def __init__(self, x, y, image):
        super().__init__(x,y,image)

    def is_obstacle(self): return True
