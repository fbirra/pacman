from food import Food

class MonsterKiller(Food):
    FOOD_POINTS = 10
    def __init__(self, x, y, image):
        super().__init__(x,y,image)

    def eat(self): return self.FOOD_POINTS

    def is_powerup(self): return True
