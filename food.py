from actor import Actor

class Food(Actor):
    FOOD_POINTS = 1
    def __init__(self, x, y, image):
        super().__init__(x,y,image)

    def is_edible(self): return True

    def eat(self): return self.FOOD_POINTS
