import pygame
import constants
from animated_actor import AnimatedActor
from spritesheet import SpriteSheet

PACMAN_LEFT = 0
PACMAN_RIGHT = 1
PACMAN_UP = 2
PACMAN_DOWN = 3
PACMAN_DYING = 4

def init_pacman_animations(sprite_sheet):
        animations = [ [], [], [], [], [] ]
        animations[PACMAN_RIGHT].append(sprite_sheet.get_image(0, 0, 16, 16))
        animations[PACMAN_RIGHT].append(sprite_sheet.get_image(16, 0, 16, 16))

        animations[PACMAN_LEFT].append(sprite_sheet.get_image(0, 16, 16, 16))
        animations[PACMAN_LEFT].append(sprite_sheet.get_image(16, 16, 16, 16))

        animations[PACMAN_UP].append(sprite_sheet.get_image(0, 32, 16, 16))
        animations[PACMAN_UP].append(sprite_sheet.get_image(16, 32, 16, 16))

        animations[PACMAN_DOWN].append(sprite_sheet.get_image(0, 48, 16, 16))
        animations[PACMAN_DOWN].append(sprite_sheet.get_image(16, 48, 16, 16))

        for i in range(2,14):
            animations[PACMAN_DYING].append(sprite_sheet.get_image(32+i*16, 0, 16, 16))

        return animations

class Pacman(AnimatedActor):

    sprite_sheet = SpriteSheet("images/spritesheet.png")
    pacman_animations = init_pacman_animations(sprite_sheet)

    def __init__(self, x, y):
        super().__init__(x, y, self.pacman_animations, PACMAN_DYING)
        self.wants_to_go_up = False
        self.wants_to_go_down = False
        self.wants_to_go_left = False
        self.wants_to_go_right = False
        self.obstacles = None

    def set_edibles(self, edibles):
        self.edibles = edibles

    def set_killers(self, killers):
        self.killers = killers

    def update(self):
        # update from parent class
        super().update()
        # test movement intentions against obstacles (maze)

        # try moving up
        if self.wants_to_go_up and self.can_go_up(): 
            self.start_animation(PACMAN_UP)

        # try moving down
        if self.wants_to_go_down and self.can_go_down(): 
            self.start_animation(PACMAN_DOWN)

        # try moving left
        if self.wants_to_go_left and self.can_go_left(): 
            self.start_animation(PACMAN_LEFT)

        #try moving right
        if self.wants_to_go_right and self.can_go_right(): 
            self.start_animation(PACMAN_RIGHT)

        # really move... if we can...
        if self.state == PACMAN_UP:
            if self.can_go_up():
                self.rect.y-=1
            else:
                self.stop_animation()
        elif self.state == PACMAN_DOWN:
            if self.can_go_down():
                self.rect.y+=1
            else:
                self.stop_animation()
        elif self.state == PACMAN_LEFT:
            if self.can_go_left():
                self.rect.x-=1
            else:
                self.stop_animation()
        elif self.state == PACMAN_RIGHT:
            if self.can_go_right():
                self.rect.x+=1
            else:
                self.stop_animation()

        # adjust pacman position (use the level as a cylinder)
        self.rect.x = self.rect.x % constants.SCREEN_WIDTH

        # lunch time
        victims = pygame.sprite.spritecollide(self, self.edibles, True)
        # if there is one element in the list (in fact there can't be more than one)
        if len(victims) > 0:
            # test if it gives the pacman the ability to stun the monsters
            if victims[0].is_powerup():
                # go through each monster
                for killer in self.killers:
                    # and stun it
                    killer.stunn()