import constants
from animated_actor import AnimatedActor

MONSTER_LEFT = 0
MONSTER_RIGHT = 1
MONSTER_UP = 2
MONSTER_DOWN = 3
MONSTER_DYING = 4
MONSTER_STUNNED = 5
MONSTER_RECOVERING = 6
# number of seconds that a monster takes to fully recover
MONSTER_TIME_TO_RECOVER = 10    
 # number of update()s that a monster takes to fully recover
MONSTER_STEPS_TO_RECOVER = constants.FPS * MONSTER_TIME_TO_RECOVER

def init_monster_animations(sprite_sheet, monster_row):
        row = 16*(monster_row + 4)
        animations = [ [], [], [], [], [], [], [] ]
        animations[MONSTER_RIGHT].append(sprite_sheet.get_image(0, row, 16, 16))
        animations[MONSTER_RIGHT].append(sprite_sheet.get_image(16, row, 16, 16))

        animations[MONSTER_LEFT].append(sprite_sheet.get_image(32, row, 16, 16))
        animations[MONSTER_LEFT].append(sprite_sheet.get_image(48, row, 16, 16))

        animations[MONSTER_UP].append(sprite_sheet.get_image(64, row, 16, 16))
        animations[MONSTER_UP].append(sprite_sheet.get_image(80, row, 16, 16))

        animations[MONSTER_DOWN].append(sprite_sheet.get_image(96, row, 16, 16))
        animations[MONSTER_DOWN].append(sprite_sheet.get_image(112, row, 16, 16))

        animations[MONSTER_STUNNED].append(sprite_sheet.get_image(128, 64, 16, 16))
        animations[MONSTER_STUNNED].append(sprite_sheet.get_image(144, 64, 16, 16))

        animations[MONSTER_RECOVERING].append(sprite_sheet.get_image(160, 64, 16, 16))
        animations[MONSTER_RECOVERING].append(sprite_sheet.get_image(176, 64, 16, 16))

        for i in range(2,14):
            animations[MONSTER_DYING].append(sprite_sheet.get_image(32+i*16, 0, 16, 16))

        return animations


class Monster(AnimatedActor):

    def __init__(self, x, y, animations, initial_state):
        super().__init__(x, y, animations, initial_state)
        self.steps_to_recover = 0

    def set_target(self, target):
        self.target = target

    def is_killer(self):
        return True

    def is_movable(self):
        return True

    def stunn(self):
        self.start_animation(MONSTER_STUNNED)
        self.steps_to_recover = MONSTER_STEPS_TO_RECOVER

    def lock_on_target(self):
        distance_x = self.target.rect.x - self.rect.x
        distance_y = self.target.rect.y - self.rect.y

        if distance_y > 0:
            if self.can_go_down():
                self.start_animation(MONSTER_DOWN)
                return
        elif distance_y < 0:
            if self.can_go_up():
                self.start_animation(MONSTER_UP)
                return
        elif distance_x > 0:
            if self.can_go_right():
                self.start_animation(MONSTER_RIGHT)
                return
        elif distance_x < 0:
            if self.can_go_left():
                self.start_animation(MONSTER_LEFT)
                return

        # choose any direction we can move
        if self.state == MONSTER_UP or self.state == MONSTER_DOWN:
            if self.can_go_left():
                self.start_animation(MONSTER_LEFT)
            elif self.can_go_right():
                self.start_animation(MONSTER_RIGHT)
            elif self.can_go_up():
                self.start_animation(MONSTER_UP)
            elif self.can_go_down():
                self.start_animation(MONSTER_DOWN)
        elif self.state == MONSTER_LEFT or self.state == MONSTER_RIGHT:
            if self.can_go_up():
                self.start_animation(MONSTER_UP)
            elif self.can_go_down():
                self.start_animation(MONSTER_DOWN)
            elif self.can_go_left():
                self.start_animation(MONSTER_LEFt)
            elif self.can_go_right():
                self.start_animation(MONSTER_RIGHT)


    def update(self):
        super().update()
        if self.will_collide(0,0):
            print("Estou tramado!")
        # check if monster is stunned
        if self.state == MONSTER_STUNNED:
            self.steps_to_recover -= 1
            if(self.steps_to_recover == 0):
                # not too much artificial intelligence in these monsters... yet 
                self.start_animation(MONSTER_LEFT)
        else:
            if self.state == MONSTER_UP:
                if not self.can_go_up():
                    self.lock_on_target()
            elif self.state == MONSTER_DOWN:
                if not self.can_go_down():
                    self.lock_on_target()
            elif self.state == MONSTER_LEFT:
                if not self.can_go_left():
                    self.lock_on_target()
            elif self.state == MONSTER_RIGHT:
                if not self.can_go_right():
                    self.lock_on_target()

        if self.state == MONSTER_UP: self.rect.y -= 1
        elif self.state == MONSTER_DOWN: self.rect.y += 1
        elif self.state == MONSTER_LEFT: self.rect.x -= 1
        elif self.state == MONSTER_RIGHT: self.rect.x += 1
