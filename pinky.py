import monster
from spritesheet import SpriteSheet

class Pinky(monster.Monster):
    sprite_sheet = SpriteSheet("images/spritesheet.png")
    pinky_animations = monster.init_monster_animations(sprite_sheet, 1)

    def __init__(self, x, y):
        super().__init__(x,y,self.pinky_animations, monster.MONSTER_RIGHT)
