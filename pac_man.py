import pygame
import constants
import colors

pygame.init()
 
# Set the width and height of the screen [width, height]

size = (constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Pac Man")

import spritesheet
#from player import Player
from levels import Level
from pacman import Pacman
from blinky import Blinky
from pinky import Pinky
from inky import Inky
from clyde import Clyde

# Loop until the user clicks the close button.
done = False
 
# Used to manage how fast the screen updates
clock = pygame.time.Clock()

pacman = Pacman(104, 204)

blinky = Blinky(86,124)
pinky = Pinky(104,124)
inky = Inky(122,124)
clyde = Clyde(104,140)

level = Level(0)

sprites_list = level.build()

powerups = []
movables = []
killers = [ blinky, pinky, inky, clyde]

obstacles = pygame.sprite.Group()
edibles = pygame.sprite.Group()
all = pygame.sprite.Group()

for sprite in sprites_list:
    if sprite.is_edible():
        edibles.add(sprite)
    if sprite.is_powerup():
        powerups.append(sprite)

    if sprite.is_obstacle():
        obstacles.add(sprite)
    if sprite.is_movable():
        movables.append(sprite)
    all.add(sprite)

pacman.set_edibles(edibles)
pacman.set_killers(killers)

movables.append(pacman)
movables.append(blinky)
movables.append(pinky)
movables.append(inky)
movables.append(clyde)

for movable in movables:
    movable.set_obstacles(obstacles)

for killer in killers:
    killer.set_target(pacman)

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if(pacman != None):
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    pacman.move_up(True)
                elif event.key == pygame.K_DOWN:
                    pacman.move_down(True)
                elif event.key == pygame.K_LEFT:
                    pacman.move_left(True)
                elif event.key == pygame.K_RIGHT:
                    pacman.move_right(True)
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_UP:
                    pacman.move_up(False)
                elif event.key == pygame.K_DOWN:
                    pacman.move_down(False)
                elif event.key == pygame.K_LEFT:
                    pacman.move_left(False)
                elif event.key == pygame.K_RIGHT:
                    pacman.move_right(False)

 
    # --- Game logic should go here
    for m in movables:
        m.update()

 
    # --- Drawing code should go here
 
    # First, clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.
    screen.fill(colors.BLACK)

    # draw all static actors 
    all.draw(screen)
    # draw all the movable actors
    for m in movables:
        m.draw(screen)

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()
 
    # --- Limit to FPS frames per second
    clock.tick(constants.FPS)
 
# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()


