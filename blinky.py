import monster
from spritesheet import SpriteSheet

class Blinky(monster.Monster):
    sprite_sheet = SpriteSheet("images/spritesheet.png")
    blinky_animations = monster.init_monster_animations(sprite_sheet, 0)

    def __init__(self, x, y):
        super().__init__(x,y,self.blinky_animations, monster.MONSTER_LEFT)
