import pygame

class Actor(pygame.sprite.Sprite):
    def __init__(self, x, y, image):
        super().__init__()
        self.image = image
        self.rect = image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def is_edible(self):
        return False

    def is_obstacle(self):
        return False

    def is_killer(self):
        return False

    def is_movable(self):
        return False

    def is_powerup(self):
        return False
