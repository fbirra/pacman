import monster
from spritesheet import SpriteSheet

class Clyde(monster.Monster):
    sprite_sheet = SpriteSheet("images/spritesheet.png")
    clyde_animations = monster.init_monster_animations(sprite_sheet, 3)

    def __init__(self, x, y):
        super().__init__(x,y,self.clyde_animations, monster.MONSTER_DOWN)
